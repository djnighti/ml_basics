#!/usr/bin/env python
# coding: utf-8

# In[3]:


import tensorflow
import keras
import pandas as pd
import numpy as np
import sklearn
from sklearn import linear_model, preprocessing
from sklearn.utils import shuffle 
from sklearn.neighbors import KNeighborsClassifier 
import matplotlib.pyplot as pyplot
from matplotlib import style
import pickle


# In[4]:


data = pd.read_csv("car.data")

print(data.head())


# In[5]:


data_col_names = list(data.columns.values)
print(data_col_names)


# In[14]:


le = preprocessing.LabelEncoder()
data1=data[data_col_names[0:len(data_col_names)-1]]
data1_col_names = list(data1.columns.values)
print(data1_col_names)
predict = data_col_names[-1]


# In[15]:


x = []
for col_name in data1_col_names:
    x.append(list(le.fit_transform(list(data[col_name]))))

# zipping list of lists (* --- https://stackoverflow.com/questions/4112265/how-to-zip-lists-in-a-list)
X = list(zip(*x))
Y = list(le.fit_transform(list(data[predict])))


# In[16]:


print(len(X))
print(len(Y))


# In[ ]:





# In[17]:


# initialize itteration variables
best = 0
max_num_neighbors = 100
optimal_num_neighbors = 1
# Peform training 10x
for train_itt in range(10):
    # check how increasing number of nearest neighbors increases accuracy 
    for num_neighbors in range(1, 2*max_num_neighbors, 2):
        # Split data for training and testing
        x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X,Y,test_size=0.1)
        # Implement KNN model
        knn_model = KNeighborsClassifier(n_neighbors=num_neighbors)
        # Apply curve fit of model prediction
        knn_model.fit(x_train, y_train)
        # Get accuracy of model performance
        accuracy = knn_model.score(x_test, y_test)
        # determine optimal model 
        if accuracy > best:
            best = accuracy
            optimal_num_neighbors = num_neighbors
            # saving model for later reference
            with open("knn_model_cars.pickle", "wb") as f:
                pickle.dump(knn_model, f)
print(f"Model Accuracy: {best}")
print(f"Optimal number of neighbors: {optimal_num_neighbors}")
pickle_in = open("knn_model_cars.pickle", "rb")
best_knn_model = pickle.load(pickle_in)


# In[18]:





# In[24]:


predictions = best_knn_model.predict(x_test)

# for x in range(len(predictions)):
#     print(f"predicted: {predictions[x]}, Data: {x_test[x]}, actual: {y_test[x]}")


# In[21]:





# In[41]:


style.use("ggplot")

col_names = list(data1.columns)
num_col = len(col_names)
# formatting data sets to be plotted with predicted values
x_test_format = list(zip(*x_test))
for col in range(num_col):
    pyplot.scatter(x_test_format[col], predictions, s=200, c='blue', label='Predicted')
    pyplot.scatter(data1[col_names[col]], data[predict], s=100, color='r', label='Actual')
    pyplot.xlabel(col_names[col])
    pyplot.ylabel(predict)
    pyplot.legend(loc="center right")
    pyplot.show()


# In[ ]:




