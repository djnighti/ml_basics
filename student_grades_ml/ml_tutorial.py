#!/usr/bin/env python
# coding: utf-8

# In[23]:


import tensorflow
import keras
import pandas as pd
import numpy as np
import sklearn
from sklearn import linear_model
from sklearn.utils import shuffle 
import matplotlib.pyplot as pyplot
from matplotlib import style
import pickle


# In[24]:


data = pd.read_csv("student-mat.csv", sep=";")


# In[25]:


print(data.head())


# In[68]:


data1 = data[['G1', 'G2', 'G3', 'studytime', 'failures', 'absences']]
data2 = data[['G1', 'G2', 'studytime', 'failures', 'absences']]


# In[69]:


print(data1)


# In[70]:


predict = "G3"
X = np.array(data1.drop([predict], 1))
Y = np.array(data1[predict])
print(X)
print("now G3")
print(Y)


# In[71]:


best = 0
for train_itt in range(100):
    x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X,Y,test_size=0.1)
    linear = linear_model.LinearRegression()
    linear.fit(x_train, y_train)
    accuracy = linear.score(x_test, y_test)
#     print(accuracy)
    
    if accuracy > best:
        best = accuracy
        with open("studentmodel2.pickle", "wb") as f:
            pickle.dump(linear, f)
print(best)


# In[72]:


# linear = linear_model.LinearRegression()
# linear.fit(x_train, y_train)


# In[73]:


# accuracy = linear.score(x_test, y_test)
# print(accuracy)


# In[74]:


# with open("studentmodel.pickle", "wb") as f:
#     pickle.dump(linear, f)

pickle_in = open("studentmodel2.pickle", "rb")
best_linear = pickle.load(pickle_in)


# In[75]:


print(f"coef: {best_linear.coef_}, Intercept: {best_linear.intercept_}")


# In[76]:


predictions = best_linear.predict(x_test)

for x in range(len(predictions)):
    print(f"predicted grade: {predictions[x]}, scores: {x_test[x]}, actual grade: {y_test[x]}")


# In[ ]:





# In[82]:


style.use("ggplot")

col_names = list(data2.columns)
num_col = len(col_names)
# formatting data sets to be plotted with predicted values
x_test_format = list(zip(*x_test))
print(col_names)
for col in range(num_col):
    pyplot.scatter(data2[col_names[col]], data1[predict])
    pyplot.plot(x_test_format[col], predictions, c='blue')
    pyplot.xlabel(col_names[col])
    pyplot.ylabel("Final grade")
    pyplot.show()


# In[ ]:





# In[ ]:




